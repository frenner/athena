index_ref
(.*)_mems(.*)
(.*)_timings(.*)
(.*)HLTNav_Summary_AODSlimmedAux(.*)
(.*)TrigCompositeAuxContainer_v2_HLTNav_Summary_ESDSlimmedAux(.*)


# Ignore most b-tagging outside the recommended taggers: DL1dv01 and GN2v01
xAOD::BTagging(?!.*PFlow.*DL1dv01_p.*)
xAOD::BTagging(?!.*PFlow.*GN2v01_p.*)

# Also ignore some b-tagging on tracking
xAOD::TrackParticleAuxContainer.*\.btagIp_.*

# Ignoring agreed at the RIG meeting on 17.6.2022 (https://its.cern.ch/jira/browse/ATLASRECTS-7101)
Muon::MuonPRD_Container_p2<Muon::MMPrepData_p1>_MM_Measurements.m_prds.m_stripNumbers
Muon::MuonPRD_Container_p2<Muon::sTgcPrepData_p1>_STGC_Measurements.m_prds.m_stripNumbers

