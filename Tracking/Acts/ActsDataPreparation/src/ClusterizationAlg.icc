/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "AthenaMonitoringKernel/Monitored.h"
#include "xAODInDetMeasurement/ContainerAccessor.h"

namespace ActsTrk {

template <typename IClusteringTool>
ClusterizationAlg<IClusteringTool>::ClusterizationAlg(const std::string& name,
				     ISvcLocator* pSvcLocator)
    : AthReentrantAlgorithm(name, pSvcLocator) {}

template <typename IClusteringTool>
StatusCode ClusterizationAlg<IClusteringTool>::initialize()
{
    ATH_MSG_DEBUG("Initializing " << name() << " ...");

    ATH_CHECK(m_rdoContainerKey.initialize());
    ATH_CHECK(m_clusterContainerKey.initialize());
    ATH_CHECK(m_roiCollectionKey.initialize());

    ATH_CHECK(m_clusteringTool.retrieve());
    ATH_CHECK(m_regionSelector.retrieve());

    ATH_CHECK(detStore()->retrieve(m_idHelper, m_idHelperName));

    // Monitoring
    ATH_CHECK(m_monTool.retrieve(EnableTool{not m_monTool.empty()}));

    //caching
    ATH_CHECK(m_ClusterCache.initialize(m_cache_enabled));
    ATH_CHECK(m_ClusterCacheBackend.initialize(m_cache_enabled));

    return StatusCode::SUCCESS;
}

template <typename IClusteringTool>
StatusCode ClusterizationAlg<IClusteringTool>::execute(const EventContext& ctx) const
{
    auto timer = Monitored::Timer<std::chrono::milliseconds>( "TIME_execute" );
    auto mon = Monitored::Group( m_monTool, timer );

    Cache_WriteHandle cacheHandle;
    if(m_cache_enabled){
      cacheHandle = Cache_WriteHandle(m_ClusterCache, ctx);
      auto updateHandle = Cache_BackendUpdateHandle(m_ClusterCacheBackend, ctx);
      ATH_CHECK(updateHandle.isValid());
      ATH_CHECK(cacheHandle.record(std::make_unique<Cache_IDC>(updateHandle.ptr())));
      ATH_CHECK(cacheHandle.isValid());
    }

    SG::ReadHandle<RDOContainer> rdoContainer = SG::makeHandle(m_rdoContainerKey, ctx);
    ATH_CHECK(rdoContainer.isValid());

    SG::WriteHandle<ClusterContainer> clusterHandle = SG::makeHandle(m_clusterContainerKey, ctx);
    ATH_CHECK(clusterHandle.record( std::make_unique<ClusterContainer>(), 
				    std::make_unique<ClusterAuxContainer>() ));
    ClusterContainer *clusterContainer = clusterHandle.ptr();
    // Reserve space, estimate of mean clusters to reduce re-allocations
    clusterContainer->reserve( m_expectedClustersPerRDO.value() * rdoContainer->size() );



    // retrieve the RoI as provided from upstream algos
    SG::ReadHandle<TrigRoiDescriptorCollection> roiCollectionHandle = SG::makeHandle( m_roiCollectionKey, ctx );
    ATH_CHECK(roiCollectionHandle.isValid());      
    const TrigRoiDescriptorCollection *roiCollection = roiCollectionHandle.cptr(); 

    // Get list of Hash Ids from the RoI
    std::vector<IdentifierHash> listOfIds;
    for (const auto* roi : *roiCollection) {
      listOfIds.clear();
      m_regionSelector->HashIDList(*roi, listOfIds);
      // We'd need to first check the id hashes have not already been processed beforehand, and only then
      // add it to the list of ids to be processed.
      for (const IdentifierHash id : listOfIds) {      
        //obtain the write handle directly when we decide to process a given idhash
        //this will ensure that proper waiting 
        Cache_IDCLock cache_wh;
        if(m_cache_enabled){
          //obtain a write handle
          auto tmp = cacheHandle->getWriteHandle(id);
          Cache_IDCLock::Swap(cache_wh, tmp);
          //check if already available
          if(cache_wh.OnlineAndPresentInAnotherView()) continue;
        }

        // If not already processed, do it now
        const RawDataCollection* rdos = rdoContainer->indexFindPtr(id);
        if (rdos != nullptr && !rdos->empty()) {
          auto prev_len = clusterContainer->size();
          ATH_CHECK(m_clusteringTool->clusterize(*rdos, *m_idHelper, ctx,*clusterContainer));

          if(m_cache_enabled){
            //add to the cache
            ATH_CHECK(Cache::Helper<BaseClusterType>::insert(cache_wh, clusterContainer, prev_len, clusterContainer->size()));
          }
        }
      } // loop on ids
    } // loop on rois
    
    return StatusCode::SUCCESS;
}


} // namespace ActsTrk
